require 'rubygems'
require 'sinatra'
require 'sinatra/reloader'
require 'pry'
require 'sqlite3'
require 'sinatra/activerecord'

set :database, { adapter: 'sqlite3', database: 'blog.db' }

class Post < ActiveRecord::Base
	has_many :comments
	validates :name, presence: true
	validates :comment, presence: true
end

class Comment < ActiveRecord::Base
	belongs_to :post
end

before do
	@posts = Post.all
	@all_comments = Comment.all
end

get '/' do

	@p = Post.new
	erb :index
end

get '/new' do
	erb :new
end


post '/new' do
	@new_posts = params
	@p = Post.new(@new_posts)

	if @p.save
		erb :index
	else
		@error = @p.errors.full_messages.first
		erb :new
	end
end



get '/details/:post_id' do
	@this_post = Post.find(params["post_id"])
	

	erb :details
end

post '/details/:post_id' do
	@new_comments = params
	@c = Comment.new(@new_comments)


	@c.save
	erb :index
end

get '/test' do
	erb :test
end

#new comments